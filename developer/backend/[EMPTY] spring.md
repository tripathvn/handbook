# Spring

## Project structure

1. Component-based structure(Root-Submodule)
    - Tree example
        **TODO**
    - Ưu: 
        * Dễ quản lý src
        * Có thể build-deploy-ci/cd lẻ từng module
        * Cấu trúc tường minh
        * Đồng bộ dependency cho toàn bộ module hay sub-module riêng
        * Linh hoạt dễ mở rộng
        * Dễ phân chia tác vụ
    - Nhược: 
        * Connect giữa các module
        * Khó triển khai và bảo trì
        * Phức tạp hơn
2. Layered structure(Single)
    - Tree example
        **TODO**
    - Ưu: 
        * Dễ hiểu và triển khai
        * Dễ bảo trì
        * Tăng tính tái sử dụng
    - Nhược: 
        * Không linh hoạt
        * Khó phân chia tác vụ

## Naming convention

1. Follow `common/naming_convention.md`
3. Đặt tên cho các gói (packages) dựa trên chức năng và mục đích của chúng, ví dụ: com.example.myapp.controller, com.example.myapp.service.
4. Sử dụng tên rõ ràng và mô tả chức năng của các API trong các phương thức điều khiển (controller), ví dụ: getUsers(), createUser().

## Error handlling

- Có thể tạo các Class exception tuỳ chỉnh
- Nên bắt exception ở lớp Service và Throw exception ra lớp controller

## Multi environment

- Không tạo profile file để phân biệt các môi trường mà sử dụng ENV Variable để cấu hình (12 factor app)
- Có thể mã hoá url/user/password db connection