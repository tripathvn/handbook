# Monorepo

For project with FEs and BEs, we will create separate repositories in gitlab and host them separately for CI/CD and management purposes

In addition to those repositories, we will create a separate repository called `monorepo` and use `git submodule` to create references to above repositories

![image-20230403103405733](./images/monorepo1.png)

This `monorepo` repository will include

- `README.md` will have some instruction on how to use  
- `docker-compose` scripts to start the whole service locally
  - Mount `./mysql/import:/docker-entrypoint-initdb.d` in docker-compose to add scripts for creating database and accounts
  - Configuration files for services that needs file override
  - `Dockerfile` for services that needs different `Dockerfile` for local testing
